USERNAME=$(whoami)
ZSH_DISABLE_COMPFIX=true

export ZSH="/Users/$USERNAME/.oh-my-zsh"
export GOPATH=$HOME/Documents/Code/go
export GOROOT="$HOME/sdk/go1.14.2"

export EXPO_TOKEN="ztumZ-t_KKTg7vSuUYALZz-NGpyXVsNtuZD5vqTA"
export GOOGLE_APPLICATION_CREDENTIALS="/Users/dkCasLar/Library/CloudStorage/OneDrive-LEGO/Technical/flowing-flame-357109-7696faffc303.json"

PATH=$PATH:$GOROOT/bin
PATH=$PATH:/Users/$USERNAME/Code/other/programs

ZSH_THEME="powerlevel9k/powerlevel9k"
POWERLEVEL9K_MODE='nerdfont-complete'

POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_PROMPT_ADD_NEWLINE=false
POWERLEVEL9K_RPROMPT_ON_NEWLINE=true
POWERLEVEL9K_RVM_BACKGROUND="black"
POWERLEVEL9K_RVM_FOREGROUND="249"
POWERLEVEL9K_RVM_VISUAL_IDENTIFIER_COLOR="red"
POWERLEVEL9K_TIME_BACKGROUND="black"
POWERLEVEL9K_TIME_FOREGROUND="white"
POWERLEVEL9K_TIME_FORMAT="\UF43A %D{%T  \UF133  %d/%m-%y}"
POWERLEVEL9K_RVM_BACKGROUND="black"
POWERLEVEL9K_RVM_FOREGROUND="249"
POWERLEVEL9K_RVM_VISUAL_IDENTIFIER_COLOR="red"
POWERLEVEL9K_STATUS_VERBOSE=false
POWERLEVEL9K_VCS_CLEAN_FOREGROUND='black'
POWERLEVEL9K_VCS_CLEAN_BACKGROUND='green'
POWERLEVEL9K_VCS_UNTRACKED_FOREGROUND='black'
POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND='orange'
POWERLEVEL9K_VCS_MODIFIED_FOREGROUND='black'
POWERLEVEL9K_VCS_MODIFIED_BACKGROUND='yellow'
POWERLEVEL9K_VCS_HIDE_TAGS='false'
POWERLEVEL9K_COMMAND_EXECUTION_TIME_BACKGROUND='black'
POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND='white'
POWERLEVEL9K_FOLDER_ICON=''
POWERLEVEL9K_STATUS_OK_IN_NON_VERBOSE=true
POWERLEVEL9K_STATUS_VERBOSE=false
POWERLEVEL9K_COMMAND_EXECUTION_TIME_THRESHOLD=0
POWERLEVEL9K_VCS_UNTRACKED_ICON='\u25CF'
POWERLEVEL9K_VCS_UNSTAGED_ICON='\u00b1'
POWERLEVEL9K_VCS_INCOMING_CHANGES_ICON='\u2193'
POWERLEVEL9K_VCS_OUTGOING_CHANGES_ICON='\u2191'
POWERLEVEL9K_VCS_COMMIT_ICON="\uf417"
POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX="%F{blue}\u256D\u2500%f"
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="%F{blue}\u2570\uf460%f "
POWERLEVEL9K_VCS_GIT_ICON=''
POWERLEVEL9K_VCS_GIT_GITHUB_ICON=''
POWERLEVEL9K_VCS_GIT_GITLAB_ICON=''
POWERLEVEL9K_VCS_GIT_BITBUCKET_ICON=''
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context custom_internet_signal  ssh root_indicator dir dir_writable vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(command_execution_time status time)
HIST_STAMPS="dd/mm/yyyy"
DISABLE_UPDATE_PROMPT=true
POWERLEVEL9K_CONTEXT_DEFAULT_BACKGROUND='black'
POWERLEVEL9K_CONTEXT_DEFAULT_FOREGROUND='purple'

plugins=(
  git 
  zsh-autosuggestions
  sudo
  history
  npm
  zsh-syntax-highlighting
)

source $ZSH/oh-my-zsh.sh

alias ll='colorls -a -l'
alias ls='colorls -a --group-directories-first -1'
alias ff='find . -not -path "**/node_modules/*" -name'
alias kaffe="(caffeinate -t 3600 -d && osascript -e 'display notification \"Vi er løbet tør for kaffe...\" with title \"Kaffe\" sound name \"funk\"') &"
alias slukkaffe='ps aux | grep "[c]affeinate -t 3600 -d" | tr -s " " | cut -d " " -f 2 | xargs kill'
alias logintodbalpha='ssh -i ~/.ssh/keyPairForDiAlpha.pem ubuntu@ec2-52-31-145-77.eu-west-1.compute.amazonaws.com'
alias logintodbprod='ssh -i ~/.ssh/keyPairForDiProd.pem ubuntu@ec2-34-245-113-140.eu-west-1.compute.amazonaws.com'
alias restartdock='defaults delete com.apple.systempreferences AttentionPrefBundleIDs; killall Dock'
alias ssh-remote="ssh -p 10022 cl@ckal.dk"
alias python='python3'
alias pip='python3 -m pip'

alias ga="git add ."
alias gc='git commit -v -m'

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/dkCasLar/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/dkCasLar/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/dkCasLar/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/dkCasLar/google-cloud-sdk/completion.zsh.inc'; fi
