## Latest MacOS

## Indstillinger
* Sprog
* Tastatur
	* Dansk layout
	* Repetitionshastighed: 8/8
	* Taster repeterer: 5/6
	* Caps lock = ESC
	* Programgenveje -> "Lås skærm", **Commad + Shift + L**
	* Touchbar
* Touch id, apple watch må låse op
* Internetkonti
	* Hjemme
	* Sekundær
	* Professionel
	* AU
* Tillad alle programmer: `sudo spctl --master-disable`
* Log ind-emner: Skjul
	* Google Drive
	* iTerm
	* Messenger
	* Fantastical
	* Magnet
	* Mail
	* Google Chrome
* Menubar:
	* Bluetooth
	* Batteriprocent
* Touchpad
    * Program expose
    * Skub mellem sider

## Finder
* Nyt vindue åbner Dokumenter
* Fjern advarsel ved tømning af papirkurv
* Indholdsoversigt
* Avanceret
    * Vis alle endelser
    * Behold mapper øverst
	* Fjern "vis advarsel ved ændring af endelse", behold mapper øverst ved sortering

## Programmer
### App Store
* 1Password
	* Lås efter 30 minutter
* Magnet
* Fantastical
	* App specific password: https://appleid.apple.com/#!&page=signin
	* Extensions -> Today add-on
	* Disable native calendar notifications
	* Show calendar weeks
	* Menu icon shows calendar week
	* App icon badge shows incomplete tasks
	* Menu bar shows next item
	* Reminders on new event
		* Timed events
			* 1 day before
			* 2 hours before
		* All-Day events
			* The day before at 18.00
			* The same day at 06.00
		* Birthday & Anniversaries
			* 7 days before at 06.00
			* The same day at 06.00	
* Messenger
* Dropover


### Internet
* Chrome: https://www.google.com/intl/da/chrome/
	* Aktivér sync
	* Youtube dark theme
	* Disable media keys: chrome://flags/#hardware-media-key-handling
* Microsoft Office: https://studerende.au.dk/it-support/software/
* AppCleaner: https://freemacsoft.net/downloads/AppCleaner_3.5.zip
* Google drive: https://www.google.com/drive/download/backup-and-sync/
* Logi Options: https://download01.logi.com/web/ftp/pub/techsupport/options/Options_8.10.64.zip
* Spotify: https://download.scdn.co/SpotifyInstaller.zip
	* Åbn ikke ved opstart
* Discord: https://discordapp.com/api/download?platform=osx
* Node.js: https://nodejs.org/dist/v12.16.1/node-v12.16.1.pkg
* Java SDK: https://www.oracle.com/java/technologies/javase-downloads.html
* VSCode: https://go.microsoft.com/fwlink/?LinkID=620882
	* Install code command
	* Settings sync plugin, **Shift + Option + D**
* Jetbrains Toolbox: https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.16.6319.dmg
* Homebrew: `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"`
* iTerm: https://iterm2.com/downloads/stable/latest
	* Import preferences: `git clone https://gitlab.com/thisbecasper/configuration.git`
* Sublime text: https://download.sublimetext.com/Sublime%20Text%20Build%203211.dmg
	* "subl" command: 
		```
		sudo su
		rm /usr/local/bin/subl
		ln -s "/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl" /usr/local/bin/subl
		exit
		```
	* settings:
		```
		"hot_exit": false,
		"remember_open_files": false
		```
	* View -> Show side menu

## Terminal:
* zsh: `chsh -s $(which zsh)`
* .zshrc: `git clone https://gitlab.com/thisbecasper/configuration.git`
* oh-my-zsh: `sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"`
* syntax-highlighting: 
	```
	git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
	```
* colorls
	* rbenv: 
		```
		brew install rbenv
		rbenv install 2.7.0
		rbenv local 2.7.0
		```
	* install:
	    ```
        xcode-select --install
        sudo gem install colorls
        ```
* nerd-font:
	```
	brew tap homebrew/cask-fonts
	brew cask install font-hack-nerd-font
	```
	* enable font in iTerm
* auto-suggestions: `git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions`
* powerlevel9k: `git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k`
* tldr: `brew install tldr`

## LEGO
* rsa key pair: `ssh-keygen -t rsa -b 4096 -C "user@device"`
* Programmer
    * Postman: https://dl.pstmn.io/download/latest/osx
    * Amazon WorkSpaces: https://d2td7dqidlhjx7.cloudfront.net/prod/global/osx/WorkSpaces.pkg
    * Teams: https://go.microsoft.com/fwlink/p/?linkid=869428&clcid=0x406&culture=da-dk&country=dk&lm=deeplink&lmsrc=groupchatmarketingpageweb&cmpid=directdownloadmac
	* DisplayLink driver: https://www.displaylink.com/downloads/file?id=1581
* Log ind-emner: Skjul
	* LEGO OneDrive: https://go.microsoft.com/fwlink/?LinkId=823060
	* Teams
* Internetkonti: LEGO
* .npmrc: `_auth=<USERNAME>:<PASSWORD> (converted to base 64)`
